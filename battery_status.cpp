#include <iostream>
#include <unistd.h>
#include <string>
#include <fstream>
#include <iomanip>


bool ANSII = false;
// Define the right colors (ANSII or hex)
#define green_font (ANSII) ? printf("\x1b[32m") : printf("^fg(#a6e22e)")
#define red_font (ANSII) ? printf("\x1b[31m") : printf("^fg(#ff0000)")
#define orange_font (ANSII) ? printf("\x1b[33m") : printf("^fg(#fc9403)")
#define reset_font (ANSII) ? printf("\x1b[0m") : printf("^fg(#ffffff)^bg(#000000)")
#define green_background (ANSII) ? printf("\x1b[42m") : printf("^fg(#ffffff)^bg(#0f9600)")
#define red_background (ANSII) ? printf("\x1b[41m") : printf("^fg(#ffffff)^bg(#ff0000)")


using namespace std;

// Define the path and battery status
string bat_path("/sys/class/power_supply/BAT1/");
string capacity_path, status_path;


// Set the paths tot the status files
int set_paths(void) {
    capacity_path = bat_path + "capacity";
    status_path = bat_path + "status";
    return 0;
}


int info(void) {
    bool skip_capacity_check = false;
    char _status[2] = {}, _capacity[4] = {};
    int capacity;
    string s_status, status, padding;
    for(;;) {  // infinite loop

        // Read the current capacity
        ifstream c(capacity_path);
        c.read(_capacity, 3);
        c.close();
        capacity = stoi(_capacity);

        // read the current status
        ifstream s(status_path);
        s.read(_status, 1);
        s.close();
        s_status = (string) _status;

        // Define the status
        if (s_status == "D") {
            status = "Discharging";
        } else if (s_status == "C") {
            status = "Charging";
        } else if (s_status == "F") {
            status = "Charged";
        } else {
            status = "Unknown";
            red_background;
            skip_capacity_check = true;
        }

        // Set colors belonging to capacity and status
        if (!skip_capacity_check) {
            switch (capacity) {
                case 0 ... 10:
                    if (s_status == "D") {
                        red_background;
                    } else {
                        red_font;
                    }
                    break;
                case 11 ... 19:
                    red_font;
                    break;
                case 20 ... 34:
                    orange_font;
                    break;
                case 35 ... 54:
                    reset_font;
                    break;
                case 55 ... 85:
                    green_font;
                    break;
                case 86 ... 95:
                    reset_font;
                    break;
                case 96 ... 100:
                    if (s_status == "C" || s_status == "F") {
                        green_background;
                    } else {
                        reset_font;
                    }
                    break;
                default:
                    red_background;
            }
        } else { skip_capacity_check = false; }

        // print status/capacity and reset string styling
        cout << setw(11) << status << " " << setw(3) << capacity << "%";
        reset_font;
        cout << endl;

        // Sleep 1 second before next iteration
        sleep(1);
    };
    return 1;
}


int main(void) {
    if (set_paths() != 0) exit(1);
    if (info() != 0) exit(1);
    exit(0);
}
