# Battery
A cpp program to output the battery status, as in chargin/discharging and capacity, with text-make-up.

## Output styling
* <10% - discharging: Red background
* 0-20%: Red
* 20-35%: Orange
* 35-55%: White
* 55-85%: Green
* 85-100%: White
* \>95% - charging: green background
